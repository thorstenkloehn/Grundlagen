package ohne_Function

import "fmt"


type Nachname struct {
	Vorname string
}

func (n   Nachname) Bearbeiten() string {
	return "Thorsten"

}

type Hund interface {
	Bearbeiten() string
}


func main() {

	var (
		srrrr = Nachname{Vorname: "Thorsten"}
	)
	bearbeiten := Hund(srrrr)
	fmt.Println(bearbeiten)

}